/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.training.transaction;

import java.util.*;

/**
 *
 * @author aakriti
 */
public class Cart {

    private long cartNumber;
    private List orderList;
    private int numberOfProducts;

    public int getNumberOfProducts() {
        return numberOfProducts;
    }

    public void setNumberOfProducts(int numberOfProducts) {
        this.numberOfProducts = numberOfProducts;
    }

    public long getCartNumber() {
        return cartNumber;
    }

    public void setCartNumber(long cartNumber) {
        this.cartNumber = cartNumber;
    }

    public List getOrderList() {
        return orderList;
    }

    public void setOrderList(List orderList) {
        this.orderList = orderList;
    }

}
