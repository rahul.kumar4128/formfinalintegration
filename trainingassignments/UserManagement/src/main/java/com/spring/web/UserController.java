package com.spring.web;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.*;
@RequestMapping("/user")
@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	
	
	@GetMapping("/all")
	public List<User> getAllUsers(){
		return userService.findAll();
	}
     @GetMapping("/login")
	public User specificUser( String name, String password) {
	return userService.findByNameAndPassword(name, password);}
	
	@PostMapping("/signup")
	public User createUser(@RequestBody User user){
		System.out.println("Creating a user" );
		System.out.println(user);
		return userService.save(user);
		}
	
	@DeleteMapping("/delete/{id}")
	public Boolean deleteById(@PathVariable Long id) {
		userService.delete(id);
		return true;
	}
	


}
