/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genericstack.stack;

import java.util.*;

/**
 *
 * @author aakriti
 */
public class Main {

    public static void main(String[] args) {

        Stack stack = new Stack();

        Object value = stack.push(10);
        System.out.println(value + " " + "is pushed");

        Object value1 = stack.push("Ram");
        System.out.println(value1 + " " + "is pushed");

        Object value2 = stack.push(new Book("Beloved", "Tony Morrison", 450.00));
        System.out.println(value2 + " " + "is pushed");

        Object value3 = stack.push(new Book("Fiction", "Ram Baugh", 700.00));
        System.out.println(value3 + " " + "is pushed");

        Object value4 = stack.pop();
        System.out.println(value4 + " " + "is popped");
    }

}
